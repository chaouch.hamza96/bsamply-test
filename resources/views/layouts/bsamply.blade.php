<!DOCTYPE html>
<html data-wf-page="5cd0a4dfc94507558498109b" data-wf-site="5cc08428d2c73015d3b08896">
<head>
  <meta charset="utf-8">
  <title> @yield('title') </title>
  <meta content="Login" property="og:title">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <link href="{{ asset('css/normalize.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('css/webflow.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('css/platform.css') }}" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/favicon.ico" rel="shortcut icon" type="image/x-icon">
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">
  <style>
    .w-select {
      background-image: none;
      background-image: none;
      border-radius: 0px; !important
    }

    #popup{
      padding: 18px
    }

    #popup p, #popup p.more{
      line-height: inherit
    }
  </style>
</head>
<body>
<div id="overlay">
    <img src="{{ asset('images/closeWhite.svg') }}" width="24" height="24" id="close" alt="" class="close">
    <div id="popup">
      <p id="message">
      </p>
    </div>
</div>
@yield('content')
<div class="_50 full-page img"></div>
<script src="https://d1tdp7z6w94jbb.cloudfront.net/js/jquery-3.3.1.min.js" type="text/javascript" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="{{ asset('js/webflow.js') }}" type="text/javascript"></script>
<script>
    function submitForm(){
      if(document.getElementById("mail").value=="" || document.getElementById("mail").value.indexOf("@")==-1)
      {
        alert("È necessario inserire la mail");
        return;
      }
      if(!document.getElementById("auth").checked)
      {
        alert("È necessario accettare la privacy policy");
        return;
      }
      if(document.getElementById("cf").value.length!=16)
      {
        alert("Lunghezza codice fiscale non valida");
        return;
      }
      if( ! /^[0-9A-Z]{16}$/.test(document.getElementById("cf").value.toUpperCase()) )
      {
        alert("Il codice fiscale contiene valori non validi");
              return;
      }
          var s = 0;
          var even_map = "BAFHJNPRTVCESULDGIMOQKWZYX";
          for(var i = 0; i < 15; i++){
              var c = document.getElementById("cf").value[i].toUpperCase();
              var n = 0;
              if( "0" <= c && c <= "9" )
                  n = c.charCodeAt(0) - "0".charCodeAt(0);
              else
                  n = c.charCodeAt(0) - "A".charCodeAt(0);
              if( (i & 1) === 0 )
                  n = even_map.charCodeAt(n) - "A".charCodeAt(0);
              s += n;
          }
          if( s%26 + "A".charCodeAt(0) !== document.getElementById("cf").value.toUpperCase().charCodeAt(15) )
          {
        alert("Codice fiscale non valido");
        return;
      }
      if(document.getElementById("tel").value.length<10)
      {
        alert("Inserire un numero di telefono valido");
        return;
      }
      if(!isNumeric(document.getElementById("tel").value))
      {
        alert("Inserire un numero di telefono valido");
        return;
      }

      //document.getElementById("form_to_submit").submit();
      //send user registration
    }

    function isNumeric(n){
      return !isNaN(parseFloat(n)) && isFinite(n);
    }

    $(document).ready(function(){
      $("#close").on("click", function(){
        $("#overlay").fadeOut();
      });
    })
  </script>
</body>
</html>