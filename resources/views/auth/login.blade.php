  @extends('../layouts.bsamply')
  
  @section('title', 'Login')

  @section('content')
  <div class="mobile">
    <div class="logoCont">
      <h1 style="text-align:left;display:inline-block"><img src="{{ asset('images/logo.svg') }}" style="width:32px;margin-right:4px" alt="">Concorsi</h1></div>
    <h4>Attualmente il portale non è disponibile da mobile, visita nuovamente questa pagina da desktop</h4>
  </div>
  <div id="overlay">
    <img src="{{ asset('images/closeWhite.svg') }}" width="24" height="24" id="close" alt="" class="close">
    <div id="popup">
      <p id="message">
      </p>
    </div>
  </div>
  <div class="_50 full-page">
    <div class="wrapper">
      <div class="search-filter-bar login w-clearfix">
        <h1 style="text-align:left;display:inline-block"><img src="{{ asset('images/logo.svg') }}" style="width:32px;margin-right:4px" alt="">Concorsi</h1>
        <h4>Bentornato, accedi al tuo account per procedere</h4>
        <form id="email-form" name="email-form" data-name="Email Form" method="POST" action="{{ route('login') }}">
        @csrf
        <div class="search-card w-form form-login">
          <div class="text-field-container no-shadow"><label for="Ricerca-card" class="label-field">Mail</label><input type="email" class="search-field-contain w-input @error('email') is-invalid @enderror" maxlength="256" name="email" placeholder="Es: 1243" id="Ricerca-card-2"></div>
          <div class="text-field-container no-shadow"><label for="Ricerca-card-3" class="label-field">Password</label><input type="password" class="search-field-contain w-input @error('password') is-invalid @enderror" maxlength="256" name="password" id="password" placeholder="**********"></div>
        </div>
        
        @if (Route::has('password.request'))
          <a href="{{ route('password.request') }}" class="button text-row no-margin w-button">Password dimenticata?</a>
        @endif
          <a href="{{route('register')}}" class="button text-row no-margin w-button button-left">Nuovo utente?</a>
        </div><input type="submit" value=" Accedi " name="submit" class="button login w-button"></div>
      </form>
    </div>
  </div>
  @endsection
