@extends('../layouts.bsamply')

@section('title', 'Register')

@section('content')
<div class="_50 full-page">
  <div class="wrapper">
    <div class="search-filter-bar login w-clearfix">
      <h1 style="text-align:left;display:inline-block"><img src="images/logo.svg" style="width:32px;margin-right:4px" alt="">Concorsi</h1>
      <h4>Benvenuto, registra il tuo account per procedere</h4>
      <form method="POST" action="{{ route('register') }}" id="form_to_submit" onsubmit="submitForm()">
      @csrf
      <div class="search-card w-form form-login">
          <div class="text-field-container no-shadow"><label class="label-field">Mail</label><input type="text" class="search-field-contain w-input" maxlength="256" name="email" placeholder="candidato@service.it" id="mail"></div>
          <div class="text-field-container no-shadow"><label class="label-field">Codice fiscale</label><input type="text" class="search-field-contain w-input" maxlength="16" name="cf" placeholder="CCCNNN00M11S222V" id="cf"></div>
          <div class="text-field-container no-shadow"><label class="label-field">Phone</label><input type="text" class="search-field-contain w-input" maxlength="12" name="tel" placeholder="01212121212" id="tel"></div>
      </div>
      <a href="{{ route('login') }}" class="button text-row no-margin w-button">Already registered?</a>
      <span id="error">
      </span>
      <input type="checkbox" id="auth"> I accept the <a href="#" target="_blank">Privacy Policy</a>
      </div><input type="submit" value=" Sign in " class="button login w-button"></div>
      </form>
  </div>
  
  @endsection