  @extends('../layouts.bsamply')

  @section('title', 'Register')
  
  @section('content')
  <div class="_50 full-page">
    <div class="wrapper">
      <div class="search-filter-bar login w-clearfix">
        <h1 style="text-align:left;display:inline-block"><img src="images/logo.svg" style="width:32px;margin-right:4px" alt="">Concorsi</h1>
        <h4>Benvenuto, registra il tuo account per procedere</h4>
        <form action="{{ route('register') }}" method="POST">
        <div class="search-card w-form form-login">
            <div class="text-field-container no-shadow"><label class="label-field">Mail</label><input type="text" class="search-field-contain w-input" maxlength="256" name="email" placeholder="candidato@service.it" id="mail"></div>
            <div class="text-field-container no-shadow"><label class="label-field">Codice fiscale</label><input type="text" class="search-field-contain w-input" maxlength="16" name="cf" placeholder="CCCNNN00M11S222V" id="cf"></div>
            <div class="text-field-container no-shadow"><label class="label-field">Phone</label><input type="text" class="search-field-contain w-input" maxlength="12" name="tel" placeholder="01212121212" id="tel"></div>
        </div>
        <a href="{{ route('login') }}" class="button text-row no-margin w-button">Already registered?</a>
        <span id="error">
        </span>
        <input type="checkbox" id="auth"> I accept the <a href="#" target="_blank">Privacy Policy</a>
        </div><input type="button" onclick="submitForm()" value=" Sign in " name="submit" class="button login w-button"></div>
        </form>
    </div>
    <div class="_50 full-page img"></div>
    <script src="https://d1tdp7z6w94jbb.cloudfront.net/js/jquery-3.3.1.min.js" type="text/javascript" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="js/webflow.js" type="text/javascript"></script>
    <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
    <script>
      function submitForm(){
        if(document.getElementById("mail").value=="" || document.getElementById("mail").value.indexOf("@")==-1)
        {
          alert("È necessario inserire la mail");
          return;
        }
        if(!document.getElementById("auth").checked)
        {
          alert("È necessario accettare la privacy policy");
          return;
        }
        if(document.getElementById("cf").value.length!=16)
        {
          alert("Lunghezza codice fiscale non valida");
          return;
        }
        if( ! /^[0-9A-Z]{16}$/.test(document.getElementById("cf").value.toUpperCase()) )
        {
          alert("Il codice fiscale contiene valori non validi");
    			return;
        }
    		var s = 0;
    		var even_map = "BAFHJNPRTVCESULDGIMOQKWZYX";
    		for(var i = 0; i < 15; i++){
    			var c = document.getElementById("cf").value[i].toUpperCase();
    			var n = 0;
    			if( "0" <= c && c <= "9" )
    				n = c.charCodeAt(0) - "0".charCodeAt(0);
    			else
    				n = c.charCodeAt(0) - "A".charCodeAt(0);
    			if( (i & 1) === 0 )
    				n = even_map.charCodeAt(n) - "A".charCodeAt(0);
    			s += n;
    		}
    		if( s%26 + "A".charCodeAt(0) !== document.getElementById("cf").value.toUpperCase().charCodeAt(15) )
    		{
          alert("Codice fiscale non valido");
          return;
        }
        if(document.getElementById("tel").value.length<10)
        {
          alert("Inserire un numero di telefono valido");
          return;
        }
        if(!isNumeric(document.getElementById("tel").value))
        {
          alert("Inserire un numero di telefono valido");
          return;
        }
        //send user registration
        });
      }

      function isNumeric(n){
        return !isNaN(parseFloat(n)) && isFinite(n);
      }

      $(document).ready(function(){
        $("#close").on("click", function(){
          $("#overlay").fadeOut();
        });
      })
    </script>

    @endsection